﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarLot.Data
{
    public class SemiTruck : Car
    {
        public SemiTruck()
        {
            Feul = FeulType.Diesel;
            Price = 50000;
            FeulMax = 50;
        }
    }
}
