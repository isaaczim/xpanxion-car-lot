﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarLot.Data
{
    public class Car
    {
        protected int FuelLevel { get; set; }
        protected int FeulMax { get; set; }
        protected FeulType Feul;
        protected int FeulUse { get; set; }
        protected bool IsHybrid { get; set; }
        protected bool WillDrive { get; set; }
        public int Sale { get; set; }
        public int Price { get; set; }

        public Car()
        {
            FeulMax = 14;
            Sale = 20;
            Price = 20000;
            if (IsHybrid)
            {
                FeulUse -= 2;
            }
        }

        public Response<string> FeulUp(int amount, FeulType feul)
        {
            Response<string> response = new Response<string>();
            if (feul == Feul)
            {
                response.Success = true;
                FuelLevel += amount;
            }
            else
            {
                response.Success = false;
                response.Data = "You have used the wrong fuel the car will no longer start.";
            }

            return response;
        }

        public Response<string> Honk()
        {
            Response<string> response = new Response<string> {Data = "Beep"};
            return response;
        }

        public Response<string> Drive()
        {
            Response<string> response = new Response<string> {Data = "Zoom"};
            if (WillDrive)
            {
                if (FuelLevel < FeulUse)
                {
                    response.Success = false;
                    response.Message = "Oh No, you are out of fuel.";
                }
                else
                {
                    FuelLevel -= FeulUse;
                    response.Success = true;
                }

            }
            else
            {
                response.Success = false;
                response.Data = "Unable to You have used the wrong fuel the car will no longer start.";
            }
            return response;
        }
    }
}
