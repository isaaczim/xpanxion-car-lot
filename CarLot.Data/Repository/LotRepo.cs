﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarLot.Data.Repository
{
    public class LotRepo
    {
        static List<Car> _cars;

        public List<Car> Inventory()
        {
            return _cars;
        }

        public Response<string> AddCar(Car car)
        {
            Response<string> response = new Response<string>();
            if (_cars.Count < 25)
            {
                _cars.Add(car);
                response.Success = true;
            }
            else
            {
                response.Success = false;
                response.Message = "To many cars in lot unable to add another car.";
            }
            return response;
        }
    }
}
