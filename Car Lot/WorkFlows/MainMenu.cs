﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Car_Lot.Utilites;

namespace Car_Lot.WorkFlows
{
    public class MainMenu
    {
        public void Exacute()
        {
            do
            {
                Console.Clear();
                Console.WriteLine
(@"*************************************************
*
*
* 1. Display Cars
*
* 2. Add Car
*
* 3. Quit
*
*
**************************************************");
                string input = UserPrompts.GetStringFromUser("\nEnter Choice: ");

                ProcessChoice(input);
            } while (true);
        }

        private void ProcessChoice(string choice)
        {
            IWorkflow _workflow;
            switch (choice)
            {
                case "1":
                    _workflow = new DisplayCarsWorkflow();
                    _workflow.Execute();
                    break;
                case "2":
                    _workflow = new AddCarWorkflow();
                    _workflow.Execute();
                    break;
                case "3":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Please use a valid input.");
                    break;
            }
        }
    }
}
