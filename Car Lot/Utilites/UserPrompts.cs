﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Car_Lot.Utilites
{
    public class UserPrompts
    {
        public static string GetStringFromUser(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }
    }
}
